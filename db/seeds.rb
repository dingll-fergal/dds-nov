url      = "https://restcountries.eu/rest/v1/all"
response = HTTParty.get(URI.encode(url))

response.each do |country|
  county      = Country.new
  county.name = country['name']
  county.code  = country['alpha2Code']
  county.save!
end

sports = ['Football','Cricket', 'Formula 1' , 'Boxing']
sports.each do |sport|
  Sport.create!(name: sport)
end

EventDate.create(description: 'Testing', event_date: Time.now, event_date_sid: 'string', created_at: Time.now, updated_at: Time.now)
bpl = open("http://vignette3.wikia.nocookie.net/topps-kick-16/images/2/22/BPL_Logo.png/revision/latest?cb=20150810170121")
Competition.create(description: 'Barclay Premier Leauge', sport_id: 1, event_date_id: 1, season_desc: 'this is testing', season_code: 'A12BZ', summer_season_flag: 'what', summer_season_desc: 'yes', summer_season_code: 'QW1Z', created_at: Time.now, updated_at: Time.now, image_file: bpl)
file = open("https://d2r1vs3d9006ap.cloudfront.net/s3_images/1147264/UEFA_Champions_League_Logo.png?1422278789")
Competition.create(description: 'Champions Leauge', sport_id: 1, event_date_id: 1, season_desc: 'this is testing', season_code: 'A12BZ', summer_season_flag: 'what', summer_season_desc: 'yes', summer_season_code: 'QW1Z', created_at: Time.now, updated_at: Time.now, image_file: file)
Competition.create(description: 'Euorpion cup', sport_id: 1, event_date_id: 1, season_desc: 'this is testing', season_code: 'A12BZ', summer_season_flag: 'what', summer_season_desc: 'yes', summer_season_code: 'QW1Z', created_at: Time.now, updated_at: Time.now)
EventType.create(type_desc:'testing', sport_id: 1, competition_id: 2, country_id: 1, duration: 2, completed: 1, abandoned: 0, created_at: Time.now, updated_at: Time.now)
EventVenue.create(venue_name: 'Pakistan', home_team_id: 1, home_team_name: 'MU', address1: 'Testting', address2: 'Testting', address3: 'Testting', country_id: 1, city: 'Lahore', postcode: 5400, phone: 123456789, capacity: 5000, owner: 'Fergal', created_at: Time.now, updated_at: Time.now)
Fixture.create(fixture_date: Time.now, home_ht: 'string', away_ht: 'string', result_ht: 'string', home_ft: 'string', away_ft: 'string', result_ft: 'string', referee: 'string', home_shots: 5, away_shots: 5, target: 5, home_frees: 4, away_frees: 5, home_red_cards: 2, away_red_cards: 6, home_yellow_cards:7, away_yellow_cards:5, home_wides: 2, away_wides: 3, competition_id: 1, event_date_id: 1, home_team_id: 2, away_team_id:1, event_venue_id: 1, created_at: Time.now, updated_at: Time.now)

logo = open("http://4.bp.blogspot.com/-1nGu7wGrbpw/U_YczrZpl1I/AAAAAAAADsg/sAM8mBY9Dhk/s1600/Logo%2BArsenal.png")
team = TeamDim.create(team_name: 'Arsenal', logo: logo, competition_id: 1, country_id: 239)
team.team_facts.create(competition_id: 1, league_position: 1,  played: 14, win: 14, draw: 0,
                       loss: 0, points: 42, goals_for: 64, goals_against: 4, goals_against: 2,
                       home_draw: 0, home_loss: 0, home_points: 21, home_goals_for: 12, home_goals_against: 3,
                       home_goal_diff: 0, away_win: 7, away_loss: 0, away_draws: 0, away_points: 21,
                       away_goals_for: 21, away_goal_against: 0, away_goal_diff: 0)

logo = open("https://upload.wikimedia.org/wikipedia/ru/7/7a/Manchester_United_FC_crest.svg")
team = TeamDim.create(team_name: 'Manchester United', logo: logo, competition_id: 1, country_id: 239)
team.team_facts.create(competition_id: 1, league_position: 2,  played: 14, win: 13, draw: 0,
                       loss: 1, points: 39, goals_for: 64, goals_against: 4, goals_against: 2,
                       home_draw: 0, home_loss: 0, home_points: 21, home_goals_for: 12, home_goals_against: 3,
                       home_goal_diff: 0, away_win: 7, away_loss: 0, away_draws: 0, away_points: 21,
                       away_goals_for: 21, away_goal_against: 0, away_goal_diff: 0)

Event.create(description: 'MU vs ARS', event_date_id: 1, event_type_id: 1, event_venue_id: 1, competition_id: 2, home_team_id: 2, away_team_id: 1, fixture_id: 1, created_at: Time.now, updated_at: Time.now)

# teams = FootballData.fetch(:competitions, :leagueTable, id: 398)['standing']
# teams.each do |t|
#   team = TeamDim.create(team_name: t['team'], competition_id: 1, country_id: 239)
#   team.team_facts.create(competition_id: 1, league_position: t['rank'].to_i,
#                          played: t['playedGames'])
# end

ars_players = FootballData.fetch(:teams, :players, id: 57)['players']
ars = TeamDim.first
ars_players.each do |p|
  country_id = Country.where(name: p['nationality']).first.present? ? Country.where(name: p['nationality']).first.id : 1
  pl = PlayerDim.create(player_name: p['name'], date_of_birth: p['dateOfBirth'],
                   country_id: country_id, jersey_number: p['jerseyNumber'])
  ars.team_players.create(player_dim_id: pl.id)
end

mu_players = FootballData.fetch(:teams, :players, id: 66)['players']

mu = TeamDim.last
mu_players.each do |p|
  country_id = Country.where(name: p['nationality']).first.present? ? Country.where(name: p['nationality']).first.id : 1
  pl = PlayerDim.create(player_name: p['name'], date_of_birth: p['dateOfBirth'],
                   country_id: country_id, jersey_number: p['jerseyNumber'])
  mu.team_players.create(player_dim_id: pl.id)
end
