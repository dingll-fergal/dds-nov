class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :description
      t.references :event_date, foreign_key: true
      t.references :event_type, foreign_key: true
      t.references :event_venue, foreign_key: true
      t.references :competition, foreign_key: true
      t.integer :home_team_id
      t.integer :away_team_id
      t.references :fixture, foreign_key: true

      t.timestamps
    end
  end
end
