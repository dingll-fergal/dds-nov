class CreateCompetitions < ActiveRecord::Migration[5.0]
  def change
    create_table :competitions do |t|
      t.string :description
      t.references :sport, foreign_key: true
      t.references :event_date, foreign_key: true
      t.string :season_desc
      t.string :season_code
      t.string :summer_season_flag
      t.string :summer_season_desc
      t.string :summer_season_code

      t.timestamps
    end
  end
end
