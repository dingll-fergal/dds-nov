class AddImageFileToCompetition < ActiveRecord::Migration[5.0]
  def change
    add_column :competitions, :image_file, :string
  end
end
