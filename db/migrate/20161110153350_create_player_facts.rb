class CreatePlayerFacts < ActiveRecord::Migration[5.0]
  def change
    create_table :player_facts do |t|
      t.references :player_dim, foreign_key: true
      t.references :competition, foreign_key: true
      t.references :event_date, foreign_key: true
      t.datetime :end_date
      t.boolean :is_current
      t.string :position

      t.timestamps
    end
  end
end
