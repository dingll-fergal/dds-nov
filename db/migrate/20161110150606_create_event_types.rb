class CreateEventTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :event_types do |t|
      t.string :type_desc
      t.references :sport, foreign_key: true
      t.references :competition, foreign_key: true
      t.references :country, foreign_key: true
      t.integer :duration
      t.boolean :completed
      t.boolean :abandoned

      t.timestamps
    end
  end
end
