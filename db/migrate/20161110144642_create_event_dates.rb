class CreateEventDates < ActiveRecord::Migration[5.0]
  def change
    create_table :event_dates do |t|
      t.string :description
      t.datetime :event_date
      t.string :event_date_sid

      t.timestamps
    end
  end
end
