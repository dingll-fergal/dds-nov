class AddLogoToTeamDim < ActiveRecord::Migration[5.0]
  def change
    add_column :team_dims, :logo, :string
  end
end
