class AddJerseyNumberToPlayerDim < ActiveRecord::Migration[5.0]
  def change
    add_column :player_dims, :jersey_number, :string
  end
end
