# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161221122756) do

  create_table "competitions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "description"
    t.integer  "sport_id"
    t.integer  "event_date_id"
    t.string   "season_desc"
    t.string   "season_code"
    t.string   "summer_season_flag"
    t.string   "summer_season_desc"
    t.string   "summer_season_code"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file"
    t.index ["event_date_id"], name: "index_competitions_on_event_date_id", using: :btree
    t.index ["sport_id"], name: "index_competitions_on_sport_id", using: :btree
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.string   "code"
    t.string   "tld"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_dates", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "description"
    t.datetime "event_date"
    t.string   "event_date_sid"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "event_types", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type_desc"
    t.integer  "sport_id"
    t.integer  "competition_id"
    t.integer  "country_id"
    t.integer  "duration"
    t.boolean  "completed"
    t.boolean  "abandoned"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["competition_id"], name: "index_event_types_on_competition_id", using: :btree
    t.index ["country_id"], name: "index_event_types_on_country_id", using: :btree
    t.index ["sport_id"], name: "index_event_types_on_sport_id", using: :btree
  end

  create_table "event_venues", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "venue_name"
    t.integer  "home_team_id"
    t.string   "home_team_name"
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.integer  "country_id"
    t.string   "city"
    t.integer  "postcode"
    t.string   "phone"
    t.integer  "capacity"
    t.string   "owner"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["country_id"], name: "index_event_venues_on_country_id", using: :btree
  end

  create_table "events", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "description"
    t.integer  "event_date_id"
    t.integer  "event_type_id"
    t.integer  "event_venue_id"
    t.integer  "competition_id"
    t.integer  "home_team_id"
    t.integer  "away_team_id"
    t.integer  "fixture_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["competition_id"], name: "index_events_on_competition_id", using: :btree
    t.index ["event_date_id"], name: "index_events_on_event_date_id", using: :btree
    t.index ["event_type_id"], name: "index_events_on_event_type_id", using: :btree
    t.index ["event_venue_id"], name: "index_events_on_event_venue_id", using: :btree
    t.index ["fixture_id"], name: "index_events_on_fixture_id", using: :btree
  end

  create_table "fixtures", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "fixture_date"
    t.string   "home_ht"
    t.string   "away_ht"
    t.string   "result_ht"
    t.string   "home_ft"
    t.string   "away_ft"
    t.string   "result_ft"
    t.string   "referee"
    t.integer  "home_shots"
    t.integer  "away_shots"
    t.integer  "target"
    t.integer  "home_frees"
    t.integer  "away_frees"
    t.integer  "home_red_cards"
    t.integer  "away_red_cards"
    t.integer  "home_yellow_cards"
    t.integer  "away_yellow_cards"
    t.integer  "home_wides"
    t.integer  "away_wides"
    t.integer  "competition_id"
    t.integer  "event_date_id"
    t.integer  "home_team_id"
    t.integer  "away_team_id"
    t.integer  "event_venue_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["competition_id"], name: "index_fixtures_on_competition_id", using: :btree
    t.index ["event_date_id"], name: "index_fixtures_on_event_date_id", using: :btree
    t.index ["event_venue_id"], name: "index_fixtures_on_event_venue_id", using: :btree
  end

  create_table "identities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "photos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.string   "image_url"
    t.string   "file"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "player_dims", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "player_name"
    t.datetime "date_of_birth"
    t.integer  "age"
    t.integer  "country_id"
    t.boolean  "retired"
    t.datetime "retirement_date"
    t.integer  "first_club_id"
    t.integer  "current_club_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "jersey_number"
    t.index ["country_id"], name: "index_player_dims_on_country_id", using: :btree
  end

  create_table "player_facts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "player_dim_id"
    t.integer  "competition_id"
    t.integer  "event_date_id"
    t.datetime "end_date"
    t.boolean  "is_current"
    t.string   "position"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["competition_id"], name: "index_player_facts_on_competition_id", using: :btree
    t.index ["event_date_id"], name: "index_player_facts_on_event_date_id", using: :btree
    t.index ["player_dim_id"], name: "index_player_facts_on_player_dim_id", using: :btree
  end

  create_table "sports", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "team_dims", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "team_name"
    t.integer  "competition_id"
    t.string   "address1"
    t.string   "address2"
    t.string   "address3"
    t.integer  "post_code"
    t.string   "home_ground"
    t.integer  "country_id"
    t.boolean  "current_flag"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "logo"
    t.index ["competition_id"], name: "index_team_dims_on_competition_id", using: :btree
    t.index ["country_id"], name: "index_team_dims_on_country_id", using: :btree
  end

  create_table "team_facts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "team_dim_id"
    t.integer  "competition_id"
    t.integer  "league_position"
    t.integer  "played"
    t.integer  "win"
    t.integer  "draw"
    t.integer  "loss"
    t.integer  "points"
    t.integer  "goals_for"
    t.integer  "goals_against"
    t.integer  "goal_diff"
    t.integer  "home_draw"
    t.integer  "home_loss"
    t.integer  "home_points"
    t.integer  "home_goals_for"
    t.integer  "home_goals_against"
    t.integer  "home_goal_diff"
    t.integer  "away_win"
    t.integer  "away_loss"
    t.integer  "away_draws"
    t.integer  "away_points"
    t.integer  "away_goals_for"
    t.integer  "away_goal_against"
    t.integer  "away_goal_diff"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["competition_id"], name: "index_team_facts_on_competition_id", using: :btree
    t.index ["team_dim_id"], name: "index_team_facts_on_team_dim_id", using: :btree
  end

  create_table "team_players", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "team_dim_id"
    t.integer  "player_dim_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "username"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "competitions", "event_dates"
  add_foreign_key "competitions", "sports"
  add_foreign_key "event_types", "competitions"
  add_foreign_key "event_types", "countries"
  add_foreign_key "event_types", "sports"
  add_foreign_key "event_venues", "countries"
  add_foreign_key "events", "competitions"
  add_foreign_key "events", "event_dates"
  add_foreign_key "events", "event_types"
  add_foreign_key "events", "event_venues"
  add_foreign_key "events", "fixtures"
  add_foreign_key "fixtures", "competitions"
  add_foreign_key "fixtures", "event_dates"
  add_foreign_key "fixtures", "event_venues"
  add_foreign_key "identities", "users"
  add_foreign_key "player_dims", "countries"
  add_foreign_key "player_facts", "competitions"
  add_foreign_key "player_facts", "event_dates"
  add_foreign_key "player_facts", "player_dims"
  add_foreign_key "team_dims", "competitions"
  add_foreign_key "team_dims", "countries"
  add_foreign_key "team_facts", "competitions"
  add_foreign_key "team_facts", "team_dims"
end
