# == Schema Information
#
# Table name: fixtures
#
#  id                :integer          not null, primary key
#  fixture_date      :datetime
#  home_ht           :string(255)
#  away_ht           :string(255)
#  result_ht         :string(255)
#  home_ft           :string(255)
#  away_ft           :string(255)
#  result_ft         :string(255)
#  referee           :string(255)
#  home_shots        :integer
#  away_shots        :integer
#  target            :integer
#  home_frees        :integer
#  away_frees        :integer
#  home_red_cards    :integer
#  away_red_cards    :integer
#  home_yellow_cards :integer
#  away_yellow_cards :integer
#  home_wides        :integer
#  away_wides        :integer
#  competition_id    :integer
#  event_date_id     :integer
#  home_team_id      :integer
#  away_team_id      :integer
#  event_venue_id    :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Fixture < ApplicationRecord
  belongs_to :competition
  belongs_to :event_date
  belongs_to :event_venue

  has_many :events
end
