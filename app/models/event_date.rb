# == Schema Information
#
# Table name: event_dates
#
#  id             :integer          not null, primary key
#  description    :string(255)
#  event_date     :datetime
#  event_date_sid :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class EventDate < ApplicationRecord
  has_many :competitions
  has_many :events
  has_many :fixtures
  has_many :player_facts
end
