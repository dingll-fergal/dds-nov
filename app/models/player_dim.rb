# == Schema Information
#
# Table name: player_dims
#
#  id              :integer          not null, primary key
#  player_name     :string(255)
#  date_of_birth   :datetime
#  age             :integer
#  country_id      :integer
#  retired         :boolean
#  retirement_date :datetime
#  first_club_id   :integer
#  current_club_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class PlayerDim < ApplicationRecord
  belongs_to :country

  has_many :player_facts
end
