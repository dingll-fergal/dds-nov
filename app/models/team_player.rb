# == Schema Information
#
# Table name: team_players
#
#  id            :integer          not null, primary key
#  team_dim_id   :integer
#  player_dim_id :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class TeamPlayer < ApplicationRecord
  belongs_to :team_dim
  belongs_to :player_dim
end
