# == Schema Information
#
# Table name: competitions
#
#  id                 :integer          not null, primary key
#  description        :string(255)
#  sport_id           :integer
#  event_date_id      :integer
#  season_desc        :string(255)
#  season_code        :string(255)
#  summer_season_flag :string(255)
#  summer_season_desc :string(255)
#  summer_season_code :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  image_file         :string(255)
#

class Competition < ApplicationRecord
  belongs_to :sport

  belongs_to :event_date
  has_many :events
  has_many :event_types
  has_many :fixtures
  has_many :player_dims
  has_many :player_facts
  has_many :team_dims
  has_many :team_facts
  has_many :photos, as: :imageable, dependent: :destroy

  mount_uploader :image_file, ImageUploader
end
