# == Schema Information
#
# Table name: team_facts
#
#  id                 :integer          not null, primary key
#  team_dim_id        :integer
#  competition_id     :integer
#  league_position    :integer
#  played             :integer
#  win                :integer
#  draw               :integer
#  loss               :integer
#  points             :integer
#  goals_for          :integer
#  goals_against      :integer
#  goal_diff          :integer
#  home_draw          :integer
#  home_loss          :integer
#  home_points        :integer
#  home_goals_for     :integer
#  home_goals_against :integer
#  home_goal_diff     :integer
#  away_win           :integer
#  away_loss          :integer
#  away_draws         :integer
#  away_points        :integer
#  away_goals_for     :integer
#  away_goal_against  :integer
#  away_goal_diff     :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class TeamFact < ApplicationRecord
  belongs_to :team_dim
  belongs_to :competition
end
