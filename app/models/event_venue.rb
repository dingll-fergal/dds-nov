# == Schema Information
#
# Table name: event_venues
#
#  id             :integer          not null, primary key
#  venue_name     :string(255)
#  home_team_id   :integer
#  home_team_name :string(255)
#  address1       :string(255)
#  address2       :string(255)
#  address3       :string(255)
#  country_id     :integer
#  city           :string(255)
#  postcode       :integer
#  phone          :string(255)
#  capacity       :integer
#  owner          :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class EventVenue < ApplicationRecord
  belongs_to :country

  has_many :fixtures
  has_many :events
end
