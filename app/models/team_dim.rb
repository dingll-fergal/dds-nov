# == Schema Information
#
# Table name: team_dims
#
#  id             :integer          not null, primary key
#  team_name      :string(255)
#  competition_id :integer
#  address1       :string(255)
#  address2       :string(255)
#  address3       :string(255)
#  post_code      :integer
#  home_ground    :string(255)
#  country_id     :integer
#  current_flag   :boolean
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  logo           :string(255)
#

class TeamDim < ApplicationRecord
  belongs_to :competition
  belongs_to :country

  has_many :team_facts
  has_many :team_players
  has_many :player_dims , through: :team_players
  has_many :events

  mount_uploader :logo, ImageUploader
end
