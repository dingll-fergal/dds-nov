# == Schema Information
#
# Table name: events
#
#  id             :integer          not null, primary key
#  description    :string(255)
#  event_date_id  :integer
#  event_type_id  :integer
#  event_venue_id :integer
#  competition_id :integer
#  home_team_id   :integer
#  away_team_id   :integer
#  fixture_id     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Event < ApplicationRecord
  belongs_to :event_date
  belongs_to :event_type
  belongs_to :event_venue
  belongs_to :competition
  belongs_to :fixture
  belongs_to :home_team, class_name: TeamDim, foreign_key: 'home_team_id'
  belongs_to :away_team, class_name: TeamDim, foreign_key: 'away_team_id'
end
