# == Schema Information
#
# Table name: photos
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  imageable_id   :integer
#  imageable_type :string(255)
#  image_url      :string(255)
#  file           :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Photo < ApplicationRecord
  belongs_to :imageable, polymorphic: true

  mount_uploader :file, ImageUploader

  def url
    image_url.present? ? image_url : file_url
  end
end
