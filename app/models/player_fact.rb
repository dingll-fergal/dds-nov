# == Schema Information
#
# Table name: player_facts
#
#  id             :integer          not null, primary key
#  player_dim_id  :integer
#  competition_id :integer
#  event_date_id  :integer
#  end_date       :datetime
#  is_current     :boolean
#  position       :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class PlayerFact < ApplicationRecord
  belongs_to :player_dim
  belongs_to :competition
  belongs_to :event_date
end
