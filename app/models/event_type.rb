# == Schema Information
#
# Table name: event_types
#
#  id             :integer          not null, primary key
#  type_desc      :string(255)
#  sport_id       :integer
#  competition_id :integer
#  country_id     :integer
#  duration       :integer
#  completed      :boolean
#  abandoned      :boolean
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class EventType < ApplicationRecord
  belongs_to :sport
  belongs_to :competition
  belongs_to :country

  has_many :events
end
