class CompetitionsController < ApplicationController
  before_action :load_competition, only: [:show, :destroy]

  def index
    @competitions = Competition.all
  end

  def show
    @events = @competition.events
    @photos = @competition.photos
    @team_dims = @competition.team_dims.includes(:team_facts)
  end

  private

  def load_competition
    @competition = Competition.find(params[:id])
  end
end
