class EventsController < ApplicationController
  before_action :load_event, only: [:show, :destroy]

  def index
  end

  def show
    @home_team = @event.home_team
    @away_team = @event.away_team
    @home_players = @home_team.player_dims
    @away_players = @away_team.player_dims
    @team_dims = @event.competition.team_dims.includes(:team_facts)
  end

  private

  def load_event
    @event = Event.find(params[:id])
    @events = @event.competition.events
  end
end
